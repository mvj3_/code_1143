1.      MonetDB is an open source column-oriented database management system.
2.      http://allenjoe.net/2012/05/08/区分两种不同类型的列存储数据库  

<pre>
Group A: Bigtable, HBase, Hypertable, and Cassandra. These four systems are not intended to be a complete list of systems in Group A — these are simply the four systems I understand the best in this category and feel the most confident discussing.
Group B: Sybase IQ, C-Store, Vertica, VectorWise,MonetDB, ParAccel, and Infobright. Again, this is not a complete list, but these are the systems from this group I know best. (Row/column hybrid systems such as Oracle orGreenplum are ignored from this discussion to avoid confusion, but the column-store aspects of these systems are closer to Group B than Group A.)
Group A: “non-relational column-store”
Group B: “relational column-store”
Group A: “sparse column-store”
Group B: “dense column-store”
</pre>